<?php

// Objectif 1 : 
// Instancier un objet véhicule et renvoyer ses informations dans le navigateur.

$milleniumFalcon = new Vehicule("Vaisseau", "LucasFilms", "gray");
echo $milleniumFalcon;

// Objectif 2 : 
// Ajouter un attribut et une méthode permettant d'indiquer la distance parcourue par le véhicule.
// (La méthode doit pouvoir afficher sa valeur dans le navigateur)

echo $milleniumFalcon->getDistanceParcourue();

// Objectif 3 (Bonus) :
// Créer une méthode 'faireUnTrajet' ayant comme paramètre le nombre de kilomètres à parcourir et pour but de faire avancer le véhicule. Le résultat doit être affiché. (Un attribut doit être ajouté à la classe).

$milleniumFalcon->faireUnTrajet(30);
$milleniumFalcon->faireUnTrajet(50);
echo $milleniumFalcon->getDistanceParcourue();

// Objectif (Bonus) 4 :
// Créer une méthode 'faireMarcheArrière' reculer le véhicule. Le véhicule est garé dans un garage et ne peut pas revenir plus loin que son point de départ.

echo "<br/>";
$milleniumFalcon->faireMarcheArriere(50);
echo "<br/>";
echo $milleniumFalcon->getDistanceParcourue();
echo "<br/>";
$milleniumFalcon->faireMarcheArriere(100);