<?php


class Vehicule
{
  // Attributs
  private $type;
  private $marque;
  private $couleur;

  // Distance parcourue depuis le garage en KM (Entier -> integer) ex: 50
  private int $distanceParcourue = 0;

  // Méthodes
  public function __construct($type, $marque, $couleur = null)
  {
    $this->type = $type;
    $this->marque = $marque;
    $this->couleur = $couleur;
  }

  public function __toString()
  {
    if ($this->couleur != null) {
      return "Ce vehicule est de type " . $this->getType() . " et de marque " . $this->marque . " et de couleur <span style='color: " . $this->couleur . "'> " . $this->couleur . " !</span>";
    } else {
      return "Ce vehicule est de type " . $this->getType() . " et de marque " . $this->marque . " !";
    }
  }

  // Getters et Setters

  /**
   * Get the value of type
   */
  public function getType()
  {
    return $this->type;
  }

  /**
   * Set the value of type
   *
   * @return  self
   */
  public function setType($type)
  {
    $this->type = $type;

    return $this;
  }

  /**
   * Get the value of marque
   */
  public function getMarque()
  {
    return $this->marque;
  }

  /**
   * Set the value of marque
   *
   * @return  self
   */
  public function setMarque($marque)
  {
    $this->marque = $marque;

    return $this;
  }

  /**
   * Get the value of couleur
   */
  public function getCouleur()
  {
    return $this->couleur;
  }

  /**
   * Set the value of couleur
   *
   * @return  self
   */
  public function setCouleur($couleur)
  {
    $this->couleur = $couleur;

    return $this;
  }

  public function getDistanceParcourue()
  {
    return $this->distanceParcourue;
  }

  public function setDistanceParcourue($km)
  {
    $this->distanceParcourue = $km;
  }

  public function faireUnTrajet($km)
  {
    $this->setDistanceParcourue($km + $this->getDistanceParcourue());

    echo "<br/>" . $this->__toString() . " et a parcouru " . $this->getDistanceParcourue() . " km depuis le garage";
  }

  public function faireMarcheArriere($km)
  {
    $this->setDistanceParcourue($this->getDistanceParcourue() - $km);

    if ($this->getDistanceParcourue() < 0) {
      echo "Crash dans mur ça craint !";
      $this->setDistanceParcourue(0);
    }

    // Exemple de ternaire :
    // $this->getDistanceParcourue() < 0 ? $this->setDistanceParcourue(0) : $this->getDistanceParcourue();

    echo "<br/>" . $this->__toString() . " et sa distance au garage est de " . $this->getDistanceParcourue() . " km";
  }
}